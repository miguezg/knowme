# KnowMe

## Setup

Para iniciar este proyecto, es necesario tener instalado docker.

### Entorno de desarrollo
Para poder ejecutar el proyecto, se debe crear un archivo `.env` en la carpeta `api` y `frontend`.

- `./api/.env`
```bash
DEBUG=api:* npm start
MONGO_DB=mongodb://mongo:27017/knowme
```
- `./frontend/.env` -> vacio

### Ejecución

#### Creación e iniciar
```bash
docker-compose up --build
ó
docker-compose up -d
```

#### Iniciar
```bash
docker-compose start
```

#### Terminar
```bash
docker-compose stop
```